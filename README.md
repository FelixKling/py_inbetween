> [!WARNING]  
> This tool is deprecated and crashes if running several processes simultaneously. Use [Chess Engine Connect](https://codeberg.org/FelixKling/ChessEngineConnect)  instead.

# py_inbetween
A program to connect UCI chess engines with custom commands.

Especially for connecting remote engines (via ssh), a dummy executable that creates the connection between a remote host and GUI is needed. In addition, it allows for custom settings ignoring the GUI, changing the engine name, or filtering the communication between GUI and engine. Using a local engine, the tool may also be used just to manipulate the engine GUI interaction.

A similar program called "inbetween" exists, but I could not find the source code and after all Python is just plain simple and I wanted to do some modifications. That is why I chose the name py_inbetween.

## Installation and usage
Copy the files py_inbetween.exe (for Ubuntu: py_inbetween, make it executable) and py_inbetween.yaml into a folder. Modify the py_inbetween.yaml with your settings (hints can be found in that file). The example yaml contains lines at the bottom for changing the evaluation (replacing the last digit of the evaluation with zero) that you can just comment out if this is not desired.

Install py_inbetween.exe as UCI engine in the GUI of choice. You can rename executable and yaml file, the tool always looks for a configuration file with the same name. If you create an ssh connection, make sure the key file has the proper rights and so on. In case of problems, test your command in the terminal first.

## Additional notes
This tool has not been tested thoroughly. Please don't hesitate to report any bugs.

