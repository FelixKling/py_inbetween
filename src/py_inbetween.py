#!/usr/bin/python3
"""
A simple program to connect UCI chess engines using for instance ssh.
Copyright (C) 2023 Dr. Felix Kling, Mainz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see https://www.gnu.org/licenses/
"""

import subprocess  # for running the engine
import threading  # for printing while waiting for input
import sys  # for information about the program and quitting
import yaml  # for reading the configuration
import re  # for reading the configuration
import os  # simple path handling
import datetime  # for logging
import time  # sleeping

# Windows
if os.name == 'nt':
    import win32gui


class ChessEngine:

    def __init__(self, configuration=None):
        """Initialising the engine, loading configuration and
        opening the process.

        Args:
            configuration (dict, optional): If the configuration should
            not be loaded from a file with a name identical to the executable,
            provide a dictionary with the same format.
            Defaults to None.
        """
        # finding the executable name (pyinstaller compatible!)
        if getattr(sys, 'frozen', False):
            self.folder = os.path.dirname(sys.executable)
            self.filename = sys.executable.replace("\\", "/").rsplit(
                "/", 1)[1].split(".")[0]
        elif __file__:
            self.folder = os.path.dirname(__file__)
            self.filename = __file__.replace("\\",
                                             "/").rsplit("/",
                                                         1)[1].split(".")[0]

        self.folder = self.folder.replace("\\", "/") + "/"

        # reading config or using the dict provided during initialisation
        if configuration:
            self.config = configuration
            self.managed = "script"
        else:
            self.config = self.read_config(self.folder, self.filename)
            self.managed = self.filename + ".yaml"
        if "logging" not in self.config:
            self.config["logging"] = False
        if self.config["logging"]:
            self.log(f"Configuration: {self.config}\n")

    def log(self, text):
        """A very simple logging function adding the date.

        Args:
            text (str): The text to log.
        """
        if self.config["logging"]:
            with open(f"{self.folder}{self.filename}.log", "a") as file:
                file.write(f"{datetime.datetime.now()}: {text}")

    def read_config(self, folder, file_name):
        """Reading the config.

        Args:
            folder (str): The folder of the tool.
            file_name (str): Its file name.

        Raises:
            e: Errors during reading the yaml.

        Returns:
            dict: A dictionary with the configuration.
        """
        # the config yaml is named like the executable.
        with open(f"{folder}{file_name}.yaml", "r") as stream:
            try:
                config = yaml.safe_load(stream)
            except Exception as e:
                self.log(f"Reading config failed with exception {e}.")
                raise e
        return config

    # main part (engine) #
    def write(self, command):
        """A function to pass commands to the engine process.

        Args:
            command (str): The input for the engine.
        """
        # locking thread to make sure the command is passed.
        with self.lock:
            self.engine.stdin.write(command + "\n")
            self.engine.stdin.flush()
        # do not wait for input if quit is called, exit now
        if command == "quit":
            self.engine.wait()
            sys.exit(0)

    def read_stdout(self):
        """
        This function prints the output after some
        replacements (id, options; see yaml -file).
        """
        while True:
            text = self.engine.stdout.readline()
            if text:
                text_raw = text
                # regex replacement for output
                if "replace_output" in self.config:
                    for pattern, replacement in self.config[
                            "replace_output"].items():
                        text = re.sub(pattern.replace("\\\\", "\\"),
                                      replacement.replace("\\\\", "\\"), text)
                # replace engine info according to yaml file
                if "id" in self.config:
                    for key, value in self.config["id"].items():
                        if text.startswith(f"id {key} "):
                            text = f"id {key} {value}\n"
                # hide managed options
                if "option" in self.config:
                    for key in self.config["option"]:
                        if text.startswith(f"option name {key} "):
                            value = self.config["option"][key]
                            text = (f"option name {key}* type combo "
                                    f"default {value} var {value}"
                                    "\n")
                if not text:
                    continue
                # logging output
                if text_raw != text:
                    self.log("Out (raw): " + text_raw)
                    self.log("Out (processed): " + text)
                else:
                    self.log("Out: " + text_raw)
                if text == 'uciok\n':
                    print(
                        f"option name * parameters set by type combo "
                        f"default {self.managed} "
                        f"var {self.managed}"
                        "\n",
                        end="",
                        flush=True)
                print(text, end="", flush=True)
                if text == 'uciok\n':
                    # set options according to yaml file
                    if "option" in self.config:
                        for key, value in self.config["option"].items():
                            self.log("In (processed): " +
                                     f"setoption name {key} value {value}\n")
                            self.write(f"setoption name {key} value {value}")
            else:
                time.sleep(0.1)

    def read_stdin(self):
        """
        Basically just a loop to read input and ignoring
        options managed by this tool.
        """
        while True:
            stdin = input()
            stdin_raw = stdin
            # regex replacements for input
            if "replace_input" in self.config:
                for pattern, replacement in self.config["replace_input"].items(
                ):
                    stdin = re.sub(pattern.replace("\\\\", "\\"),
                                   replacement.replace("\\\\", "\\"), stdin)
            if "option" in self.config:
                for key in self.config["option"]:
                    if stdin.startswith(f"setoption name {key} "):
                        stdin = ""
                        break
            if stdin != stdin_raw:
                self.log("In (raw): " + stdin + "\n")
                self.log("In (processed): " + stdin + "\n")
            else:
                self.log("In: " + stdin + "\n")
            if stdin:
                self.write(stdin)

    def hide_console_window(self):
        """
        On Windows, a console window is opened after loading the engine.
        Chessbase seems to expect a console application, so pyinstaller
        with --no-console does not work. This is the work-around.
        Returns:
            Bool: True if a console window was found and closed,
                  False otherwise
        """
        if os.name == 'nt':
            # the function for the processes
            def callback(hwnd, result):
                # visible?
                if win32gui.IsWindowVisible(hwnd):
                    window_title = win32gui.GetWindowText(hwnd)
                    # name of the executable
                    executable_name = sys.executable
                    # # window class?
                    # class_string = win32gui.GetClassName(hwnd)
                    # real window?
                    left, top, right, bottom = win32gui.GetWindowRect(hwnd)
                    if (right - left and bottom - top
                            # and executable_name is window_title
                            and window_title == executable_name):
                        # # this is the terminal class...
                        # and class_string == 'CASCADIA_HOSTING_WINDOW_CLASS'
                        result.append(hwnd)
                return True

            # the list of terminal windows
            win_list = []
            # populate list
            win32gui.EnumWindows(callback, win_list)
            if win_list:
                # the first item is the top-most, we hide it (0 is hidden)
                win32gui.ShowWindow(win_list[0], 0)
                return True
            return False
        return False

    def run(self):
        """Run the process and threads for reading from and writing to
        the engine

        Raises:
            e: An error if something fails.
        """
        # starting engine executable
        # prepare arguments for Windows
        si = None
        env = None
        creationflags = 0
        if os.system == "nt":
            si = subprocess.STARTUPINFO()
            si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
            env = os.environ
            creationflags = (subprocess.CREATE_NO_WINDOW
                             | subprocess.DETACHED_PROCESS)

        self.engine = subprocess.Popen(
            self.config["command"],
            shell=True,
            universal_newlines=True,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.DEVNULL,
            bufsize=1,
            cwd=self.folder,
            startupinfo=si,
            env=env,
            # otherwise you see a command line window popping up under Windows.
            creationflags=creationflags,
        )

        # non-blocking reading
        os.set_blocking(self.engine.stdout.fileno(), False)

        # block process when sending input to engine
        self.lock = threading.Lock()
        try:
            # start reading the engine stdout
            threading.Thread(target=self.read_stdout, daemon=True).start()

            # processing input
            threading.Thread(target=self.read_stdin, daemon=True).start()

            # check if engine closed and exit
            # also check if Windows opened a console window
            # during the first seconds
            console_window_checks = 0
            max_console_window_checks = 100
            while True:
                time.sleep(0.1)
                if console_window_checks < max_console_window_checks:
                    # check if window can be closed
                    if self.hide_console_window():
                        console_window_checks = max_console_window_checks
                if not self.engine.poll() is None:
                    sys.exit(0)
        except Exception as e:
            self.log(e)
            raise e


if __name__ == "__main__":
    chess_engine = ChessEngine()
    chess_engine.run()
